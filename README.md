# Crashkurs Coding

## Computerprogramm & Computersprache
Um Computern mitzuteilen, was sie für uns tun sollen, können wir Computerprogramme bzw. Code schreiben. Das bezeichnen wir als Software. Diese Programme müssen wir in einer Sprache schreiben, die Computer verstehen können. Eine Computersprache ist zum Beispiel HTML oder auch CSS, die beide vor allem für Webseiten wichtig sind.

## Software & Hardware
Unter Software verstehen wir alles Digitale, also Musikdateien, Handy-Fotos, Betriebssysteme (Windows, Linux u.a.) oder Computeranwendungen (Firefox Browser, Word-Programm, u.a.) und Apps. Zu Hardware zählen alle "harten" Gegenstände wie Handys, Computer, Kabel, USB-Sticks und so weiter.

## Bits & Bytes
Bits sind die Einheit für Computer. 1 Bit kann entweder Null oder Eins sein, also entweder Ja oder Nein, An
oder Aus. Die Information von 8 Bits sind 1 Byte, also 1 Bit * 8 = 1 Byte.

## Mensch vs. Computer
Computer tun nur exakt das, womit wir Menschen sie beauftragen. Mitterweile sind Computer außerdem sehr schnell und sehr leistungsstark, sie können also in sehr kurzer Zeit sehr komplizierte Aufgaben gleichzeitig für uns erledigen. Oben drauf sind sie besser darin zufällig zu handeln, z.B. ein Passwort zufällig zu erstellen. Menschen können sich im Gegensatz zu Computern in andere Menschen hineinversetzen, haben Emotionen und sind in der Lage selbständig zu lernen und sich zu entwickeln. Fairness, Moral und Liebe können nur wir Menschen empfinden.

## Weiter lernen
* Mehr zu CSS: https://cssclass.es/materials
* http://tutorials.codebar.io/ und
* https://www.freecodecamp.org/

## Gruppen zum Gemeinsamen Lernen und Austausch:
* Open Tech School, https://www.opentechschool.org/berlin/
* Code curious (ehemals Rails Girls Berlin), http://railsgirlsberlin.de/
* Heart of Code, http://heartofcode.org/
* CodeBar, https://codebar.io/berlin
* Crypto Party, https://www.cryptoparty.in/berlin

## Online-Kurse (oft kostenpflichtig):
* Codecademy: https://www.codecademy.com/
* Treehouse: https://teamtreehouse.com/
* Coursera: https://de.coursera.org/
* Udemy: https://www.udemy.com/